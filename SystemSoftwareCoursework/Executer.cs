﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SystemSoftwareCoursework
{
    class Executer //Класс выполняющий введенные команды
    {
        private string inputString;
        private Lexer lex; //Лексер
        private Parser prs; //Парсер
        private static Dictionary<string, int> valueTable = new Dictionary<string, int>(); //Таблица переменных
        private static Dictionary<Part, int> mathResultsTable = new Dictionary<Part, int>(); //Таблица результатов вычислений

        public Executer(string input)
        {
            this.inputString = input;
            if (inputString != "end") //Выполнить только если введенная строка не "end"
            {
                lex = new Lexer(inputString + "\n"); //Проводим лексический анализ
                prs = new Parser(lex); //Проводим синтаксический анализ
                Execute(prs.getParts()); //Выполнить
            }

        }

        private void Execute(List<Part> parts) //Выполнение
        {
            foreach (Part prt in parts) //Для всех добавленных терминалов\токенов
            {
                Token tok = null;
                Terminal term = null;
                //Приведение типов
                if (prt.getType() >= 10)
                    term = (Terminal)prt;
                else
                    tok = (Token)prt;

                //
                if (tok == null) //Если текущая часть НЕ токен
                {
                    //=====================================================MATH================================================
                    //
                    //
                    if (term.getType() == Terminal.math || term.getType()==Terminal.brackets)
                    {
                        int a = 0, b = 0, res = 0; //Временные переменные
                        bool hasSign = false;


                        if (term.getPart(1).getType() != Terminal.math && term.getPart(1).getType() != Token.variable && term.getPart(1).getType() != Token.constant && term.getPart(1).getType() != Terminal.brackets) //Если первый терминал не математика, не константа и не скобки
                        {
                            if (term.getPart(0).getType() == Terminal.math || term.getPart(0).getType() == Terminal.brackets) //Если нулевой терминал - арифметика
                            {
                                Execute(new List<Part>(1) { term.getPart(0) });
                            }
                            if (term.getPart(2).getType() == Terminal.math || term.getPart(2).getType() == Terminal.brackets) //Если последний терминал - арифметика
                            {
                                Execute(new List<Part>(1) { term.getPart(2) });
                            }



                            //A
                            if (term.getPart(0).getType() == Token.constant) //Если первый токен - константа
                            {
                                a = Convert.ToInt16(term.getPart(0).getValue());  //Парсим токен на число 
                            }
                            else if (term.getPart(0).getType() == Token.variable) //Если первый токен - переменная
                            {
                                if (valueTable.ContainsKey(term.getPart(0).getValue())) //Если в таблице переменных данная переменная присутствует
                                {
                                    a = valueTable[term.getPart(0).getValue()]; //то используем ее значение
                                    ConsoleOutput.printSuccess("Found variable '" + term.getPart(0).getValue() + "' in values table.");
                                }
                                else
                                    ConsoleOutput.printError("Variable '" + term.getPart(0).getValue() + "' not found");
                            }
                            else if (term.getPart(0).getType() == Terminal.math || term.getPart(0).getType() == Terminal.brackets) //Если первый токен - это терминал арифметики или скобок
                            {
                                if (mathResultsTable.ContainsKey(term.getPart(0)))
                                {
                                    ConsoleOutput.printSuccess("Found math result for terminal " + term.getPart(0).getValue());
                                    a = mathResultsTable[term.getPart(0)]; //Вытаскиваем значение из таблицы результатов вычислений
                                }
                                else
                                    ConsoleOutput.printError("Math result for terminal " + term.getPart(0).getValue() + " not found.");
                            }


                            //B
                            if (term.getPart(2).getType() == Token.constant)
                            {
                                b = Convert.ToInt16(term.getPart(2).getValue());
                            }
                            else if (term.getPart(2).getType() == Token.variable) //Если второй токен - переменная
                            {
                                if (valueTable.ContainsKey(term.getPart(2).getValue())) //Если в таблице переменных данная переменная присутствует
                                {
                                    b = valueTable[term.getPart(2).getValue()]; //Вытаскиваем знчение из таблицы переменных
                                    ConsoleOutput.printSuccess("Found variable '" + term.getPart(2).getValue() + "' in values table.");
                                }
                                else
                                    ConsoleOutput.printError("Variable '" + term.getPart(2).getValue() + "' not found");
                            }
                            else if (term.getPart(2).getType() == Terminal.math || term.getPart(2).getType()==Terminal.brackets) //Если второй токен - это терминал арифметики или скобок
                            {
                                if (mathResultsTable.ContainsKey(term.getPart(2))) //Если значение содержится в таблице результатов вычислений
                                {
                                    ConsoleOutput.printSuccess("Found math result for terminal " + term.getPart(2).getValue());
                                    b= mathResultsTable[term.getPart(2)]; //то вытаскиваем это значение
                                }
                                else
                                    ConsoleOutput.printError("Math result for terminal " + term.getPart(2).getValue() + " not found.");
                            }


                            //res
                            if (term.getPart(1).getValue().Contains("+"))
                            {
                                res = a + b;
                                Console.WriteLine(res);
                            }
                            else if (term.getPart(1).getValue().Contains("-"))
                            {
                                res = a - b;
                                Console.WriteLine(res);
                            }
                            else if (term.getPart(1).getValue().Contains("*"))
                            {
                                res = a * b;
                                Console.WriteLine(res);
                            }
                            else if (term.getPart(1).getValue().Contains("/"))
                            {
                                if (b != 0) //Проверка деления на ноль
                                {
                                    res = a / b;
                                    Console.WriteLine(res);
                                }
                                else
                                {
                                    ConsoleOutput.printError("Division error");
                                    break;
                                }
                            }

                        }
                        else if (term.getPart(0).getType() == Token.leftBracket && term.getPart(1).getType() == Terminal.math && term.getPart(2).getType()== Token.rightBracket) //Если идет терминал вида ([арифметика])
                        {
                            Execute(new List<Part>(1) { term.getPart(1) });
                            res = mathResultsTable[term.getPart(1)]; //Вычисляем арифметику и создаем локальную переменную с результатов, которая занесется в таблицу результатов вычислений
                        }
                        else if (term.getPart(1).getType() == Terminal.brackets) //если перед скобками стоит знак -
                        {
                            Execute(new List<Part>(1) { term.getPart(1) });
                            res = mathResultsTable[term.getPart(1)] * (term.getPart(0).getValue().Contains("+") ? 1 : -1);
                            hasSign = true;
                        }
                        else if (term.getPart(1).getType() == Token.constant) //если перед константой знак минус
                        {
                            res = Convert.ToInt16(term.getPart(1).getValue()) * (term.getPart(0).getValue().Contains("+") ? 1 : -1);
                            hasSign = true;
                        }
                        else if (term.getPart(1).getType() == Token.variable) //если перед переменной знак минус
                        {
                            res = Convert.ToInt16(valueTable[term.getPart(1).getValue()]) * (term.getPart(0).getValue().Contains("+") ? 1 : -1);
                            hasSign = true;
                        }
                        else
                            ConsoleOutput.printError("Math can't be solved");

                        if (hasSign) //Если выражение имеет знак
                        {
                            Terminal temp = new Terminal(12, term.getParts());
                            mathResultsTable.Add(term, res);
                        }
                        else
                        {
                            if (mathResultsTable.ContainsKey(term))
                                mathResultsTable[term] = res;
                            else
                                mathResultsTable.Add(term, res);
                        }
                    }
                    
                    //=====================================================INITIALIZATION================================================
                    //
                    //
                    if (term.getType() == Terminal.init) //Если текущая часть - это терминал инициализации
                    {
                        if (term.getPart(2).getType() == Token.constant) //Если [переменная]=[константа]
                        {
                            if(valueTable.ContainsKey(term.getPart(0).getValue()))
                                valueTable[term.getPart(0).getValue()]=Convert.ToInt16(term.getPart(2).getValue());
                            else
                                valueTable.Add(term.getPart(0).getValue(),Convert.ToInt16(term.getPart(2).getValue()));
                            ConsoleOutput.printSuccess("Variable '" + term.getPart(0).getValue() + "' was initialized with value " + term.getPart(2).getValue()); 
                        }
                        else if (term.getPart(2).getType() == Terminal.math || term.getPart(2).getType()==Terminal.brackets) //Если [переменная]=[арифметика]
                        {
                            Execute(new List<Part>(1) { term.getPart(2) });

                            if (valueTable.ContainsKey(term.getPart(0).getValue()))
                                valueTable[term.getPart(0).getValue()] = mathResultsTable[term.getPart(2)];
                            else
                                valueTable.Add(term.getPart(0).getValue(), mathResultsTable[term.getPart(2)]);

                            ConsoleOutput.printSuccess("Variable '" + term.getPart(0).getValue() + "' was initialized with value " + mathResultsTable[term.getPart(2)]);
                        }
                        else if (term.getPart(2).getType() == Token.variable) //Если [переменная]=[переменная]
                        {
                            if (valueTable.ContainsKey(term.getPart(2).getValue())) //Если в таблице переменных данная переменная присутствует
                            {
                                int tempVar = valueTable[term.getPart(2).getValue()]; //Берем значение из таблицы переменных
                                ConsoleOutput.printSuccess("Found variable '" + term.getPart(2).getValue() + "' in values table.");

                                if (valueTable.ContainsKey(term.getPart(0).getValue()))
                                    valueTable[term.getPart(0).getValue()] = tempVar;
                                else
                                    valueTable.Add(term.getPart(0).getValue(), tempVar); //И заносим в таблицу новую переменную с полученным значением

                                ConsoleOutput.printSuccess("Variable '" + term.getPart(0).getValue() + "' was initialized with value " + tempVar); 
                            }
                            else
                                ConsoleOutput.printError("Variable '" + term.getPart(2).getValue() + "' not found");
                        }
                    }
                }
            }
        }

        public void setInputString(string input)
        {
            this.inputString = input;
        }

        public Dictionary<string, int> getValueTable()
        {
            return valueTable;
        }
    }
}
