﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SystemSoftwareCoursework
{
    class ConsoleOutput //Класс для вывода сообщений в консоль
    {
        
        public static void printError(string err)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(err);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void printSuccess(string suc)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(suc);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void printProgress(string pr)
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine(pr);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
