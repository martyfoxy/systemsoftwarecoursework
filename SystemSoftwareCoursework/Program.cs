﻿/*Курсовая работа по Системному Программному Обеспечению
 * Выполнена студентов Чудаевым Вячеславом группы ИВБ-4-12
 * МГТУ МИРЭА 2015 год
 * 
 * Описание: 
 * Последние изменения: 27.05.15
 * */

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;
using System.Threading;
namespace SystemSoftwareCoursework
{
    class Program
    {
        private static string inputString = ""; //Входная строка
        private static Dictionary<string,int> valueTable=new Dictionary<string,int>(); //Таблица переменных
        private static Executer exec; //Вычислитель

        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("ru-RU");
            ConsoleOutput.printProgress("Данная программа является курсовым проектом по Системному Программному Обеспечению. Выполнена студентом 3 курса МГТУ МИРЭА, группы ИВБ-4-12 Чудаевым Вячеславом в мае 2015 года.\n\tВведите команду. Стоп-фраза: end\n");
            while(inputString!="end") //Стоп-фраза "end"
            {
                inputString = Console.ReadLine();
                exec = new Executer(inputString);
            }

            valueTable = exec.getValueTable();

            Console.WriteLine("\n\n\n\nFinal variables table:\n");

            foreach (KeyValuePair<string, int> kv in valueTable) //Вывести таблицу переменных
                ConsoleOutput.printSuccess(kv.Key.ToString()+" "+kv.Value.ToString());

        }
    }
}
