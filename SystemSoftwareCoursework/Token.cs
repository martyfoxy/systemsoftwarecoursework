﻿using System;

namespace SystemSoftwareCoursework
{
    class Token: Part //Класс токена. Здесь идет приведение определенному символу своего значения. Например, 5 - это константа, а ) - это левая скобка.
    {
        public const int variable = 0, constant = 1, assign = 2, mathHighSign = 3, mathLowSign = 4, leftBracket = 5, rightBracket = 6, space = 7;

        private int type;
        private string value;

        public Token(int type, string value)
        {
            this.type = type;
            this.value = value.Replace("\n","");
        }

        public override int getType()
        {
            return type;
        }

        public override string getTypeName()
        {
            switch (type)
            {
                case(variable): return "variable";
                case(constant): return "constant";
                case(assign): return "assign";
                case(mathHighSign): return "high priority math sign";
                case(mathLowSign): return "low priority math sign";
                case(leftBracket): return "left bracket";
                case(rightBracket): return "right bracket";
                case(space): return "space";
                default: return "";
            }
        }

        public override string getValue()
        {
            return value;
        }
    }
}
