﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SystemSoftwareCoursework
{
    class Terminal: Part //Класс терминала. Здесь идет приведение некоторому набору токенов своего значения. Например, a=1 - это определение, а 4+8 - это арифметика
    {
        public const int init = 10, math = 11, brackets = 12;

        private int type;
        private List<Part> value;

        public Terminal(int type, List<Part> value)
        {
            this.type = type;
            this.value = value;
        }

        public override int getType()
        {
            return type;
        }

        public override string getTypeName()
        {
            switch (type)
            {
                case(init): return "initialization";
                case(math): return "math";
                case (brackets): return "brackets";
                default: return "";
            }
        }

        public Part getPart(int i)
        {
            if (i < value.Count)
                return value[i];
            else
                return null;
        }

        public override string getValue()
        {
            string str = "";
            foreach (Part prt in value)
                str += prt.getValue();
            return str;
        }

        public List<Part> getParts()
        {
            return value;
        }
    }
}
