﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SystemSoftwareCoursework
{
    abstract class Part //Класс объединяющий Token и Terminal
    {
        abstract public int getType();
        abstract public string getTypeName();
        abstract public string getValue();
    }
}
