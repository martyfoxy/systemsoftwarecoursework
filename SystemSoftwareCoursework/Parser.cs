﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SystemSoftwareCoursework
{
    class Parser //Класс парсера. Строит из токенов терминалы.    
    {
        private Lexer lx;
        private bool error;
        private List<Part> parts;

        public Parser(Lexer lx)
        {
            error = false;
            this.lx = lx;

            parts = new List<Part>();

            foreach (Token tk in lx.getTokens())
                parts.Add(tk);

            if (!lx.everythingIsNotFine())
                execute();
        }

        private void execute()
        {
            ConsoleOutput.printProgress("===================Parser started=================\n");
            int option = 0;

            while (option != 4)
            {
                bool sign = false, high = false, low = false, brackets = false;
                switch (option)
                {
                    case (0):
                        {
                            for (int i = 0; i < parts.Count - 2; i++)
                                sign = sign || foundSign(i); //Поиск знака числа
                            if (!sign)
                            {
                                ++option; //Если не нашел, идем дальше
                                continue;
                            }
                            else
                            {
                                break; //Если нашел, то выходим из свича
                            }
                        }
                    case (1):
                        {
                            for (int i = 0; i < parts.Count - 2; i++)
                                high = high || foundHighPriorityMath(i); //Поиск знаков умножения и деления
                            if (!high)
                            {
                                ++option; //Если не нашел, идем дальше
                                continue;
                            }
                            else
                            {
                                option = 0;
                                break; //Если нашел, то выходим из свича
                            }
                        }
                    case (2):
                        {
                            for (int i = 0; i < parts.Count - 2; i++)
                                low = low || foundLowPriorityMath(i); //Поиск знаков сложения и вычитания
                            if (!low)
                            {
                                ++option; //Если не нашел, идем дальше
                                continue;
                            }
                            else
                            {
                                option = 0;
                                break; //Если нашел, то выходим из свича
                            }
                        }
                    case (3):
                        {
                            for (int i = 0; i < parts.Count - 2; i++)
                                brackets = brackets || foundBrackets(i); //Поиск скобок
                            if (!brackets)
                            {
                                ++option; //Если не нашел, идем дальше
                                continue;
                            }
                            else
                            {
                                option = 0;
                                break; //Если нашел, то выходим из свича
                            }
                        }
                    default: continue;
                }
            }
                bool flag = true;
                while (flag)
                {
                    flag = false;
                    for (int i = 0; i < parts.Count-2; i++)
                        flag = flag || foundInit(i);
                }

                foreach (Part prt in parts)
                    ConsoleOutput.printSuccess("Terminal " + prt.getTypeName() + " found with value " + prt.getValue());

                ConsoleOutput.printProgress("===================Parser ended===================\n");



            
        }

        private bool foundHighPriorityMath(int index) //Нахождение арифметики высшего приоритета
        {
            if (parts[index].getType() == Token.constant || parts[index].getType() == Token.variable || parts[index].getType() == Terminal.brackets || parts[index].getType() == Terminal.math)
            {
                if (parts[index + 1].getType() == Token.mathHighSign)
                {
                    if (parts[index + 2].getType() == Token.constant || parts[index + 2].getType() == Token.variable || parts[index + 2].getType() == Terminal.math || parts[index + 2].getType() == Terminal.brackets)
                    {
                        List<Part> module = new List<Part>(3);
                        module.Add(parts[index]);
                        module.Add(parts[index + 1]);
                        module.Add(parts[index + 2]);
                        parts.RemoveAt(index);
                        parts.RemoveAt(index);
                        parts[index] = new Terminal(Terminal.math, module);
                        ConsoleOutput.printSuccess("Math found " + parts[index].getValue());
                        return true;
                    }
                }
            }
            return false;
        }

        private bool foundLowPriorityMath(int index) //Нахождение арифметики низшего приоритета
        {
            if (parts[index].getType() == Token.constant || parts[index].getType() == Token.variable || parts[index].getType() == Terminal.brackets || parts[index].getType() == Terminal.math)
            {
                if (parts[index + 1].getType() == Token.mathLowSign)
                {
                    if (parts[index + 2].getType() == Token.constant || parts[index + 2].getType() == Token.variable || parts[index + 2].getType() == Terminal.math || parts[index + 2].getType() == Terminal.brackets)
                    {
                        List<Part> module = new List<Part>(3);
                        module.Add(parts[index]); //Добавить констанку, переменную, скобки или арифметику
                        module.Add(parts[index + 1]); //Добавить знак
                        module.Add(parts[index + 2]); //Добавить констанку, переменную, скобки или арифметику
                        parts.RemoveAt(index); //Удалить 
                        parts.RemoveAt(index); //первые два элемента из общего списка 
                        parts[index] = new Terminal(Terminal.math, module); //и вместо него добавить временный список
                        ConsoleOutput.printSuccess("Math found " + parts[index].getValue());
                        return true;
                    }
                }
            }
            return false;
        }

        private bool foundInit(int index) //Нахождение инитиализации
        {
            if (parts[index].getType() == Token.variable) //Первым токеном идет переменная
            {
                if (parts[index+1].getType() == Token.assign) //Вторым токеном идет знак равенства 
                {
                    if (parts[index + 2].getType() == Token.constant || //Третим токеном идет константа
                        parts[index + 2].getType() == Token.variable || //или переменная
                        parts[index + 2].getType() == Token.mathLowSign || //или знак плюс\минус 
                        parts[index + 2].getType() == Terminal.math || //или арифметическое выражение
                        parts[index + 2].getType() == Terminal.brackets) //или выражение в скобках
                    {
                        List<Part> module = new List<Part>(3);
                        module.Add(parts[index]);
                        module.Add(parts[index+1]);
                        module.Add(parts[index+2]);
                        parts.RemoveAt(index);
                        parts.RemoveAt(index);
                        parts[index] = new Terminal(Terminal.init, module);
                        ConsoleOutput.printSuccess("Assignment found " + parts[index].getValue());
                        return true;
                    }
                }
            }
            return false;
        }

        private bool foundBrackets(int index) //Нахождение скобок
        {
            if (parts[index].getType() == Token.leftBracket)
            {
                if (parts[index + 1].getType() == Token.constant || parts[index + 1].getType() == Token.variable || parts[index + 1].getType() == Terminal.math)
                {
                    if (parts[index + 2].getType() == Token.rightBracket)
                    {
                        List<Part> module = new List<Part>(3);
                        module.Add(parts[index]);
                        module.Add(parts[index + 1]);
                        module.Add(parts[index + 2]);
                        parts.RemoveAt(index);
                        parts.RemoveAt(index);
                        parts[index] = new Terminal(Terminal.brackets, module);
                        ConsoleOutput.printSuccess("Brackets found " + parts[index].getValue());
                        return true;
                    }
                }
            }
            return false;
        }

        private bool foundSign(int index) //Нахождение знака выражения
        {
            if (parts[index].getType() == Token.assign || parts[index].getType() == Token.mathHighSign)
            {
                if (parts[index+1].getType()==Token.mathLowSign)
                {
                    if (parts[index + 2].getType() == Terminal.brackets || parts[index + 2].getType() == Token.variable || parts[index + 2].getType()==Token.constant)
                    {
                        List<Part> module = new List<Part>(2);
                        module.Add(parts[index + 1]); //Добавить знак минус
                        module.Add(parts[index + 2]); //Добавить скобки, переменную или константу
                        parts.RemoveAt(index+1); //Удалить знак
                        parts[index+1] = new Terminal(Terminal.math, module); //И заменить это на полученное
                        ConsoleOutput.printSuccess("Sign found " + parts[index + 1].getValue());
                        return true;
                    }
                }
            }
            return false;
        }

        public Part getPart(int i)
        {
            if (i < parts.Count)
                return parts[i];
            return null;
        }

        public Lexer getLexer()
        {
            return lx;
        }

        public bool EverythingIsNotFine()
        {
            return error;
        }

        public List<Part> getParts()
        {
            return parts;
        }

    }
}
