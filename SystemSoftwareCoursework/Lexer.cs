﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace SystemSoftwareCoursework
{
    class Lexer //Класс лексера. Проверяет введенное выражение на правильность и добавляет токены в таблицу
    {
        private string inputString;
        private static List<string> patternList=new List<string>(); //Список паттернов
        private static List<Token> tokens; //Список токенов
        private static Regex rgx; 
        private static Match match;
        private static bool error;
        private static bool _instance=false;

        //===========Список паттернов==================
        private static string varPtr = @"^[a-zA-Z]+$"; //Переменная
        private static string constPtr = @"^[0-9]+$"; //Константа
        private static string assingPtr = @"^=$"; //Присвоение
        private static string outPtr = @"^out$"; //Вывод

        private static string mathHighPtr = @"^[\*\/]$"; //Арифметика высшего приоритета
        private static string mathLowPtr = @"^[\+\-]$"; //Арифметика низшего приоритета

        private static string leftBracketPtr = @"^\($"; //Левая скобка
        private static string rightBracketPtr = @"^\)$"; //Правая скобка
        private static string spacePtr = @"^ $"; //Пробел

        //============Конструктор========================
        public Lexer(String inputString)
        {                
            tokens = new List<Token>();
            error = false;
            this.inputString = inputString;
            if (!_instance)  //Добавляем паттерны только при создании первого экземпляра
            {
                patternList.Add(varPtr);
                patternList.Add(constPtr);
                patternList.Add(assingPtr);
                patternList.Add(mathHighPtr);
                patternList.Add(mathLowPtr);
                patternList.Add(leftBracketPtr);
                patternList.Add(rightBracketPtr);
                patternList.Add(spacePtr);
            }

            _instance = true;
            execute();
        }

        //==================Анализировать=================
        private void execute()
        {
            ConsoleOutput.printProgress("===================Lexer started==================\n");
            bool last = false; 
            string unanalyzed = inputString;
            string analyzing = "";
            int to = 0;
            int lastMatchNum=0; //Порядковый номер в списке паттернов
            bool flag = false;

            while (!last) //Пока не прочитана вся входная строка
            {
                to++;
                bool isMatch = false;
                analyzing = unanalyzed.Substring(0, to); //Берем подстроку, начиная с 0 символа и постепенно посимвольно добавляем символы из исходной строки
                last = unanalyzed.Length == analyzing.Length; //

                for (int i = 0; i < patternList.Count; i++) //Пробегаем по всему списку паттернов
                {
                    rgx = new Regex(patternList[i]);
                    match = rgx.Match(analyzing); //В текущей подстроке пытаемся найти строку по паттерну
                    isMatch = match.Success || isMatch;
                    if (match.Success)
                    {
                        lastMatchNum = i;
                        break;
                    }
                }

                if (!flag && !isMatch) //Если вообще ничего не найдено в текущей подстроке, то выводим ошибку синтаксиса
                {
                    error = true;
                    break;
                }

                if (flag && !isMatch) //Если до этого что-то найдено, а сейчас нет
                {
                    tokens.Add(new Token(lastMatchNum, unanalyzed.Substring(0, to - 1)));
                    unanalyzed = unanalyzed.Substring(to - 1, unanalyzed.Length-(analyzing.Length-1));
                    to = 0;
                }

                if ((flag && last) || (isMatch && last)) //Если проанализированны все символы и все в порядке
                {
                    tokens.Add(new Token(lastMatchNum, unanalyzed.Substring(0, to))); 
                }

                flag = isMatch; //флаг для следующего цикла           
            }

            if (error)
            {
                ConsoleOutput.printError("=======Syntax error! Unexpected symbol: '" + analyzing + "'=======");
            }
            else
                foreach (Token tk in tokens)
                    ConsoleOutput.printSuccess("Token " + tk.getTypeName() + " with value " + tk.getValue());
            ConsoleOutput.printProgress("===================End of lexer===================");
        }

        public Token getToken(int num) //Получить токен с определенным номером
        {
            if (num < tokens.Count)
                return tokens[num];
            else
                return null;
        }

        public List<Token> getTokens() //Получить список токенов
        {
            return tokens;
        }

        public void setInputString(string str) 
        {
            this.inputString = str;
        }

        public string getInputString()
        {
            return this.inputString;
        }

        public bool everythingIsNotFine() //Если есть ошибка
        {
            return error;
        }
    }
}
